# Commerce MoMo Payments

Commerce MoMo Payments provides 3 more payment types (MoMo Wallet, MoMo ATM, MoMo Credit Card) for Commerce payment system.

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the following modules:

- [Commerce](https://www.drupal.org/project/commerce)
- Commerce Payment

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Please add new payment gateway after installing module (/admin/commerce/config/payment-gateways).

##Supporting organizations:

- [WeebPal](https://www.drupal.org/weebpal)