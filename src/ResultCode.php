<?php

namespace Drupal\commerce_momo_payments;

class ResultCode {

  // Transaction Successful.
  const SUCCESS = 0;

  /**
   * System is under maintenance.
   * Please retry after the maintenance is over.
   */
  const SYSTEM_UNDER_MAINTENANCE = 10;

  /**
   * Access denied.
   * Merchant settings issue.
   * Please check your settings in M4B portal, or contact MoMo for configurations.
   */
  const ACCESS_DENIED = 11;

  /**
   * Unsupported API version for this request.
   * Please upgrade to our latest version of payment gateway
   * as the current version is no longer in support.
   */
  const API_VERSION_UNSUPPORTED = 12;

  /**
   * Merchant authentication failed.
   */
  const AUTHENTICATION_FAILED = 13;

}
