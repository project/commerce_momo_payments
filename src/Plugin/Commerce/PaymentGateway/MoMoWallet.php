<?php

namespace Drupal\commerce_momo_payments\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Provides MoMo Wallet gateway.
 *
 * @CommercePaymentGateway(
 *    id = "commerce_momo_wallet",
 *    label = @Translation("MoMo Wallet"),
 *    display_label = @Translation("MoMo Wallet"),
 *    forms = {
 *      "offsite-payment" = "Drupal\commerce_momo_payments\PluginForm\MoMoPaymentsForm",
 *    },
 *    payment_type = "momo_payments",
 * )
 */
class MoMoWallet extends MoMoOffsitePaymentGatewayBase implements MoMoWalletInterface  {

  /**
   * {@inheritdoc}
   */
  public function getPayUrl(PaymentInterface $payment, $request_type = 'captureWallet') {
    return parent::getPayUrl($payment, $request_type);
  }

}
