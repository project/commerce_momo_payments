<?php

namespace Drupal\commerce_momo_payments\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Provides MoMo pay with ATM gateway.
 *
 * @CommercePaymentGateway(
 *    id = "commerce_momo_pay_with_atm",
 *    label = @Translation("MoMo Pay With ATM"),
 *    display_label = @Translation("MoMo Pay With ATM"),
 *    forms = {
 *      "offsite-payment" = "Drupal\commerce_momo_payments\PluginForm\MoMoPaymentsForm",
 *    },
 *    payment_type = "momo_payments",
 * )
 */
class MoMoPayWithATM extends MoMoOffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function getPayUrl(PaymentInterface $payment, $request_type = 'payWithATM') {
    return parent::getPayUrl($payment, $request_type);
  }

}
