<?php

namespace Drupal\commerce_momo_payments\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Provides MoMo pay with Credit Card gateway.
 *
 * @CommercePaymentGateway(
 *    id = "commerce_momo_credit_card",
 *    label = @Translation("MoMo Credit Card (Visa/Mastercard/JCB)"),
 *    display_label = @Translation("MoMo Credit Card (Visa/Mastercard/JCB)"),
 *    forms = {
 *      "offsite-payment" = "Drupal\commerce_momo_payments\PluginForm\MoMoPaymentsForm",
 *    },
 *    payment_type = "momo_payments",
 * )
 */
class MoMoCreditCard extends MoMoOffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function getPayUrl(PaymentInterface $payment, $request_type = 'payWithCC') {
    return parent::getPayUrl($payment, $request_type);
  }

}
