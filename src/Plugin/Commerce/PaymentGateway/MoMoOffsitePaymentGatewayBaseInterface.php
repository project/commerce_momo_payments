<?php

namespace Drupal\commerce_momo_payments\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;

interface MoMoOffsitePaymentGatewayBaseInterface {

  /**
   * All parameters for MoMo request and response.
   */
  const ACCESS_KEY = 'accessKey';
  const PARTNER_CODE = 'partnerCode';
  const SUB_PARTNER_CODE = 'subPartnerCode';
  const STORE_NAME = 'storeName';
  const STORE_ID = 'storeId';
  const REQUEST_ID = 'requestId';
  const AMOUNT = 'amount';
  const ORDER_ID = 'orderId';
  const ORDER_INFO = 'orderInfo';
  const ORDER_GROUP_ID = 'orderGroupId';
  const REDIRECT_URL = 'redirectUrl';
  const IPN_URL = 'ipnUrl';
  const REQUEST_TYPE = 'requestType';
  const EXTRA_DATA = 'extraData';
  const ITEMS = 'items';
  const DELIVERY_INFO = 'deliveryInfo';
  const USER_INFO = 'userInfo';
  const REFERENCE_ID = 'referenceId';
  const AUTO_CAPTURE = 'autoCapture';
  const LANG = 'lang';
  const SIGNATURE = 'signature';

  /**
   * Additional keys for response.
   */
  const ORDER_TYPE = 'orderType';
  const TRANS_ID = 'transId';
  const MESSAGE = 'message';
  const RESPONSE_TIME = 'responseTime';
  const PAY_TYPE = 'payType';
  const RESULT_CODE = 'resultCode';
  const PAY_URL = 'payUrl';

  /**
   * Get return page url.
   *
   * @return \Drupal\Core\Url
   */
  public function getReturnUrl(OrderInterface $order);

  /**
   * Create new payment and get payUrl from MoMo API.
   *
   * @return string|null
   */
  public function getPayUrl(PaymentInterface $payment, $request_type = 'captureWallet');

  /**
   * Generate signature from required params and client's secret.
   *
   * @param array $params
   * @param string $secret
   *
   * @return string
   */
  public function generateSignature(array $params, string $secret_key);

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param array $data
   *
   * @return boolean
   */
  public function verifySignature(OrderInterface $order, array $data);

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return string
   */
  public function getOrderInfo(OrderInterface $order);

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return string
   *   Encode base64 of json data.
   */
  public function getExtraData(OrderInterface $order);

}
