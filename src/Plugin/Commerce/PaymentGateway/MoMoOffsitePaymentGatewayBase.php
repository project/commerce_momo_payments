<?php

namespace Drupal\commerce_momo_payments\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_momo_payments\ResultCode;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class MoMoOffsitePaymentGatewayBase extends OffsitePaymentGatewayBase implements MoMoOffsitePaymentGatewayBaseInterface {

  /**
   * Drupal\Core\Logger\LoggerChannel.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.channel.commerce_momo_payments');
    /** @var \Drupal\Core\Http\ClientFactory $http_client_factory */
    $http_client_factory = $container->get('http_client_factory');
    $instance->httpClient = $http_client_factory->fromOptions(['base_uri' => $configuration['api_endpoint']]);
    $instance->rounder = $container->get('commerce_price.rounder');
    $instance->messenger = $container->get('messenger');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = [
      'api_endpoint' => '',
      'partner_code' => '',
      'access_key' => '',
      'secret_key' => '',
      'redirect_method' => 'get',
    ];
    return $configuration + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['partner_code'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Partner code'),
      '#default_value' => $this->configuration['partner_code'] ?? '',
    ];
    $form['access_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Key'),
      '#default_value' => $this->configuration['access_key'] ?? '',
      '#required' => TRUE,
    ];
    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#default_value' => $this->configuration['secret_key'] ?? '',
      '#required' => TRUE,
    ];
    $form['api_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Endpoint'),
      '#default_value' => $this->configuration['api_endpoint'] ?? '',
      '#required' => TRUE,
      '#description' => t('Change to Dev (Test) / Production (Live) API Endpoint as per mode selected above.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['api_endpoint'] = $values['api_endpoint'];
      $this->configuration['partner_code'] = $values['partner_code'];
      $this->configuration['access_key'] = $values['access_key'];
      $this->configuration['secret_key'] = $values['secret_key'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPayUrl(PaymentInterface $payment, $request_type = 'captureWallet') {
    $order = $payment->getOrder();

    // Use this id for requestId and orderId because
    $request_id = $order->id() . '_' . date('ymdHis');

    $amount = $this->rounder->round($payment->getAmount());
    $params = [
      self::PARTNER_CODE => $this->configuration['partner_code'],
      self::ACCESS_KEY => $this->configuration['access_key'],
      self::REQUEST_ID => $request_id,
      self::AMOUNT => $amount->getNumber(),
      self::ORDER_ID => $request_id,
      self::ORDER_INFO => $this->getOrderInfo($order),
      self::REDIRECT_URL => $this->getReturnUrl($order)->toString(),
      self::IPN_URL => $this->getNotifyUrl()->toString(),
      self::REQUEST_TYPE => $request_type,
      self::EXTRA_DATA => $this->getExtraData($order)
    ];
    $params[self::SIGNATURE] = $this->generateSignature($params, $this->configuration['secret_key']);
    $params[self::LANG] = 'vi';

    try {
      $response_contents = $this->httpClient->post('create',
        [
          'json' => $params,
          'headers' => ['Content-Type' => 'application/json; charset=UTF-8']
        ]
      )->getBody()->getContents();
      $response = Json::decode($response_contents);
    }
    catch (GuzzleException $e) {
      throw new PaymentGatewayException($e->getMessage());
    }
    if (empty($response)) {
      throw new PaymentGatewayException('Cannot get pay url from MoMo API.');
    }
    if ($response[self::RESULT_CODE] != ResultCode::SUCCESS) {
      throw new PaymentGatewayException("{$response[self::MESSAGE]}({$response[self::RESULT_CODE]})");
    }

    return $response[self::PAY_URL];
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $params = $request->query->all();
    if (!isset($params['resultCode'])) {
      throw new PaymentGatewayException('No response from payment API.');
    }
    if ($params['resultCode'] != ResultCode::SUCCESS) {
      $this->messenger->addError($params['message']);
      throw new PaymentGatewayException($params['message']);
    }
    if (!$this->verifySignature($order, $params)) {
      throw new PaymentGatewayException('Invalid signature.');
    }

    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => 'authorization',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id' => $order->id(),
      'remote_id' => $params['transId'],
      // MoMo doesn't provide payment status,
      // but it should be 'completed' here because the result code is 0
      'remote_state' => 'completed',
    ]);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->messenger
      ->addError($this->t('You have canceled checkout at @gateway.', [
        '@gateway' => $this->getDisplayLabel(),
      ]), 'error');
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $params = $request->request->all();
    if (!isset($params['resultCode'])) {
      throw new PaymentGatewayException('No response from payment API.');
    }
    if ($params['resultCode'] != ResultCode::SUCCESS) {
      $this->messenger->addError($params['message']);
      throw new PaymentGatewayException($params['message']);
    }
    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->loadByRemoteId($params['transId']);
    if (!$payment) {
      throw new PaymentGatewayException("Payment entity not found.");
    }
    $order = $payment->getOrder();
    if (!$this->verifySignature($order, $params)) {
      throw new PaymentGatewayException('Invalid signature.');
    }
    // @todo: should verify amount and currency
    $payment->setState('completed');

    return new Response('', 204);
  }

  /**
   * {@inheritdoc}
   */
  public function getReturnUrl(OrderInterface $order) {
    return Url::fromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE]);
  }

  /**
   * {@inheritdoc}
   */
  public function generateSignature(array $params, $secret_key) {
    ksort($params);
    $signature_string = $this->generateStringFromArray($params);
    return hash_hmac('sha256', $signature_string, $secret_key);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderInfo(OrderInterface $order) {
    $order_info = ['Order Id: ' . $order->id()];
    foreach ($order->getItems() as $order_item) {
      $order_info[] = $order_item->label();
    }
    return implode(';', $order_info);
  }

  /**
   * {@inheritdoc}
   */
  public function getExtraData(OrderInterface $order) {
    $extra_data['email'] = $order->getEmail();
    $extra_data['orderId'] = $order->id();
    $extra_data_json = Json::encode($extra_data);
    return base64_encode($extra_data_json);
  }

  /**
   * {@inheritdoc}
   */
  public function generateStringFromArray(array $data) {
    $string = '';
    foreach ($data as $key => $value) {
      $string .= (!empty($string) ? '&' : '') . $key . '=' . $value;
    }
    return $string;
  }

  /**
   * {@inheritdoc}
   */
  public function verifySignature(OrderInterface $order, array $data) {
    if (!isset($data[self::REQUEST_ID])) {
      return FALSE;
    }

    $params = [
      self::PARTNER_CODE => $data[self::PARTNER_CODE],
      self::ACCESS_KEY => $this->configuration['access_key'],
      self::REQUEST_ID => $data[self::REQUEST_ID],
      self::AMOUNT => $data[self::AMOUNT],
      self::ORDER_ID => $data[self::ORDER_ID],
      self::ORDER_INFO => $data[self::ORDER_INFO],
      self::ORDER_TYPE => $data[self::ORDER_TYPE],
      self::TRANS_ID => $data[self::TRANS_ID],
      self::MESSAGE => $data[self::MESSAGE],
      self::RESPONSE_TIME => $data[self::RESPONSE_TIME],
      self::PAY_TYPE => $data[self::PAY_TYPE],
      self::EXTRA_DATA => $data[self::EXTRA_DATA],
      self::RESULT_CODE => $data[self::RESULT_CODE],
    ];
    $signature = $this->generateSignature($params, $this->configuration['secret_key']);
    if (!isset($data[self::SIGNATURE]) || $data[self::SIGNATURE] != $signature) {
      return FALSE;
    }

    return TRUE;
  }

}
