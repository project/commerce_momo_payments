<?php

namespace Drupal\commerce_momo_payments\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provides the MoMo payment type.
 *
 * @CommercePaymentType(
 *   id = "momo_payments",
 *   label = @Translation("MoMo Payments"),
 * )
 */
class MomoPayments extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
