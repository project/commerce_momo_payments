<?php

namespace Drupal\commerce_momo_payments\PluginForm;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide payment form for MoMo Payments.
 *
 * @package Drupal\commerce_momo_payments\PluginForm
 */
class MoMoPaymentsForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * Drupal\Core\Logger\LoggerChannel.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  public $logger;

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  public $messenger;

  /**
   * MoMoWalletForm constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel
   *   LoggerChannelInterface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   MessengerInterface.
   */
  public function __construct(LoggerChannelInterface $logger_channel, MessengerInterface $messenger) {
    $this->logger = $logger_channel;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.channel.commerce_momo_payments'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $error_redirect_url = Url::FromRoute('commerce_checkout.form', [
      'absolute' => TRUE,
      'step' => 'review',
      'commerce_order' => $payment->getOrderId(),
    ])->toString();

    try {
      $plugin = $payment->getPaymentGateway()->getPlugin();
      $pay_url = $plugin->getPayUrl($payment);
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      $this->messenger->addError('An error occurred while processing your payment request.');
      throw new NeedsRedirectException($error_redirect_url);
    }

    return $this->buildRedirectForm($form, $form_state, $pay_url, [], 'get');
  }

}
